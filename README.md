# @vasiliy0s/ts-experiments-sorting

## Usage

### Preparing

```sh
yarn install
```

### Development

```sh
yarn start
```

### Build

```sh
yarn build
```
