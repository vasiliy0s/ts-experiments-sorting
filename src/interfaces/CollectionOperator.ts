import { Comparable } from './Comparable'
import { Printable } from './Printable'
import { Swappable } from './Swappable'

export interface CollectionOperator extends Printable, Comparable, Swappable {
  readonly length: number
}
