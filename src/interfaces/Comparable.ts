import { ComparisonResult } from './Comparator'

export interface Comparable {
  compare(i: number, j: number): ComparisonResult
}
