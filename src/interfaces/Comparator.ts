export type ComparisonResult = -1 | 0 | 1

export interface Comparator<T> {
  compare(a: T, b: T): ComparisonResult
}
