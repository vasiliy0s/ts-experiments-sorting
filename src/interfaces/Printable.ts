export interface Printable {
  print(maxLength?: number, filler?: string): string
}
