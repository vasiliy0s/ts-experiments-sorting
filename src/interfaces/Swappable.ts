export interface Swappable {
  swap(i: number, j: number): void
}
