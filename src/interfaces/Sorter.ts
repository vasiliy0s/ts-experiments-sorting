import { Printable, Sortable } from '.'

/**
 * The decorator for collection sorting (`Sortable`) and
 * results printing (`Printable`)
 */
export type Sorter = Sortable & Printable
