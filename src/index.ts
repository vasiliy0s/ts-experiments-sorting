import { BubbleSort, NativeSort } from './sorters'
import {
  CharsCollection,
  CharsLinkedList,
  NumbersCollection,
  NumbersLinkedList,
} from './collection-operators'
import { printDivider, sortNPrint } from './utils'
import { charsGenerator, numbersGenerator } from './generators'

// Numbers
const numCollections: number[][] = [
  [-7, 48, -4, -10, -44, 42, 28, -32, -45, -5],
  numbersGenerator(10),
]
numCollections.forEach((numbers) => {
  printDivider(`Numbers: ${numbers.join(', ')}`, true)
  sortNPrint('Native Sort (numbers)', new NativeSort(numbers))
  sortNPrint(
    'Bubble Sort (numbers)',
    new BubbleSort(new NumbersCollection(numbers))
  )
  sortNPrint(
    'Bubble Sort Linked List (numbers)',
    new BubbleSort(new NumbersLinkedList(numbers))
  )
  printDivider()
})

// Chars
const charCollections: string[] = ['dXeiOjasAfab', charsGenerator(12)]
charCollections.forEach((chars) => {
  printDivider(`Chars: [${chars.split('').join(', ')}]`, true)
  sortNPrint('Native Sort (chars)', new NativeSort(chars))
  sortNPrint('Bubble Sort (chars)', new BubbleSort(new CharsCollection(chars)))
  sortNPrint(
    'Bubble Sort Linked List (chars)',
    new BubbleSort(new CharsLinkedList(chars))
  )
  printDivider()
})
