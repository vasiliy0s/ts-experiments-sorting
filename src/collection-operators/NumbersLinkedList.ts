import { NumbersComparator } from '../comparators'
import { LinkedListOperator } from './base'

export class NumbersLinkedList extends LinkedListOperator<number> {
  constructor(data: number[]) {
    super(data, new NumbersComparator())
  }
}
