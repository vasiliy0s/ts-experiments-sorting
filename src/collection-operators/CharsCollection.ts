import { StringComparator } from '../comparators'
import { CollectionOperator } from '../interfaces'
import { ComparableArrayHandler } from './base'

export class CharsCollection
  extends ComparableArrayHandler<string>
  implements CollectionOperator {
  constructor(data: string) {
    super(Array.from(data), new StringComparator())
  }
}
