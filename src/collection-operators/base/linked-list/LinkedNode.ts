import { Printable } from '../../../interfaces'

export class LinkedNode<T> implements Printable {
  next: LinkedNode<T> | null = null
  constructor(public value: T) {}

  print(): string {
    return [
      `LinkedNode<${typeof this.value}>:`,
      `[${this.value}]->${this.next?.value}`,
    ].join(' ')
  }
}
