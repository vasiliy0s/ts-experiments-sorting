import { assert } from 'console'
import { NoNodeAtIndexError, OutOfBoundsError } from '../../../errors'
import {
  CollectionOperator,
  Comparator,
  ComparisonResult,
} from '../../../interfaces'
import { CollectionPrinter } from '../CollectionPrinter'
import { LinkedNode } from './LinkedNode'
import { PRINT_DEBUG } from '../../../config'

export class LinkedListOperator<T>
  extends CollectionPrinter<T>
  implements CollectionOperator {
  topNode: LinkedNode<T> | null = null

  readonly length: number

  protected get collection(): T[] {
    const values: T[] = []

    let node = this.topNode
    while (node != null) {
      values.push(node.value)
      node = node.next
    }

    return values
  }

  constructor(data: ArrayLike<T>, private comparator: Comparator<T>) {
    super()

    let counter = 0
    let prevNode: LinkedNode<T> | null = null
    let curNode: LinkedNode<T>

    for (const val of data instanceof Array ? data : Array.from(data)) {
      curNode = new LinkedNode(val)
      if (!prevNode) this.topNode = curNode
      else prevNode.next = curNode
      prevNode = curNode
      counter++
    }
    this.length = counter

    if (PRINT_DEBUG)
      assert(
        this.collection.length === this.length,
        `LinkedListOperator<${typeof prevNode?.value}>: Collection length should be equal to counter!`
      )
  }

  // WARNING: Not a good choice to use this structure for swapping,
  // since `getNodes()` is O(N) -> swap() has O(N), but can be O(1)
  swap(i: number, j: number): void {
    this.validate(i)
    this.validate(j)

    if (i === j) return

    const [iNode, jNode] = this.getNodes(i, j)

    const tmpValue = jNode.value
    jNode.value = iNode.value
    iNode.value = tmpValue
  }

  compare(i: number, j: number): ComparisonResult {
    this.validate(i)
    this.validate(j)

    const [iNode, jNode] = this.getNodes(i, j)

    if (iNode == null || jNode == null) {
      throw new NoNodeAtIndexError(iNode == null ? i : j, this.print())
    }

    return this.comparator.compare(iNode.value, jNode.value)
  }

  private getNodes(...indecies: number[]): LinkedNode<T>[] {
    const indeciesLength = indecies.length
    const nodes: LinkedNode<T>[] = new Array(indeciesLength).fill(null)

    const indeciesMap = indecies.reduce(
      (acc, index, position) =>
        acc.set(index, (acc.get(index) || []).concat(position)),
      new Map<number, number[]>()
    )

    let index = 0
    let found = 0
    let node = this.topNode
    while (node && index <= this.length && found < indeciesLength) {
      if (indeciesMap.has(index)) {
        for (const pos of indeciesMap.get(index) || []) {
          nodes[pos] = node
          found++
        }
      }
      node = node.next
      index++
    }

    return nodes
  }

  private validate(index: number) {
    if (index >= this.length || index < 0) {
      throw new OutOfBoundsError(index, this.length)
    }
  }
}
