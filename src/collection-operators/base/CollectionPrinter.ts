import { Printable } from '../../interfaces'

export abstract class CollectionPrinter<T> implements Printable {
  private readonly lengthPrefixOffset = 6
  protected abstract get collection(): T[]

  print(lengthPrefixMaxLength = 6, lengthPrefixFiller = '.') {
    const col = this.collection

    const prefix = `<${col.length}>`
    const body = `[${col.join(', ')}]`

    return `${prefix}${body}`.padStart(
      lengthPrefixMaxLength + this.lengthPrefixOffset,
      lengthPrefixFiller
    )
  }
}
