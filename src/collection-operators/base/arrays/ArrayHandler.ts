import { Printable, Swappable } from '../../../interfaces'
import { CollectionPrinter } from '../CollectionPrinter'
import { OutOfBoundsError } from '../../../errors'

export abstract class ArrayHandler<T>
  extends CollectionPrinter<T>
  implements Swappable, Printable {
  protected collection: T[]

  constructor(data: T[]) {
    super()
    this.collection = data.slice()
  }

  get length() {
    return this.collection.length
  }

  swap(i: number, j: number): void {
    this.validateIndex(i)
    this.validateIndex(j)

    const tmp = this.collection[j]
    this.collection[j] = this.collection[i]
    this.collection[i] = tmp
  }

  protected validateIndex(index: number) {
    if (index > this.length || index < 0) {
      throw new OutOfBoundsError(index, this.length)
    }
  }
}
