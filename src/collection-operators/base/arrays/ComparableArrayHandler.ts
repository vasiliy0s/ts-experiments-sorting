import { Comparable, Comparator, ComparisonResult } from '../../../interfaces'
import { ArrayHandler } from './ArrayHandler'

export abstract class ComparableArrayHandler<T>
  extends ArrayHandler<T>
  implements Comparable {
  constructor(data: T[], private comparator: Comparator<T>) {
    super(data)
  }

  compare(i: number, j: number): ComparisonResult {
    this.validateIndex(i)
    this.validateIndex(j)
    return this.comparator.compare(this.collection[i], this.collection[j])
  }
}
