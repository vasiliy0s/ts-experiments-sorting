import { NumbersComparator } from '../comparators'
import { CollectionOperator } from '../interfaces'
import { ComparableArrayHandler } from './base'

export class NumbersCollection
  extends ComparableArrayHandler<number>
  implements CollectionOperator {
  constructor(data: number[]) {
    super(data, new NumbersComparator())
  }
}
