import { StringComparator } from '../comparators'
import { LinkedListOperator } from './base'

export class CharsLinkedList extends LinkedListOperator<string> {
  constructor(data: ArrayLike<string>) {
    super(data, new StringComparator())
  }
}
