export const numbersGenerator = (size: number, range = 100): number[] =>
  Array(size)
    .fill(0)
    .map(() => Math.round(Math.random() * range - range / 2))
