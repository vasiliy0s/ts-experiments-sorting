type CharRange = [number, number]

const DEFAULT_ABC_RANGE: CharRange[] = [
  [65, 90],
  [97, 122],
]

export const charsGenerator = (
  size: number,
  ranges: CharRange[] = DEFAULT_ABC_RANGE
): string =>
  String.fromCharCode(
    ...new Array(size).fill(0).map(() => {
      const range = ranges[Math.floor(Math.random() * ranges.length)]
      return range[0] + Math.round(Math.random() * (range[1] - range[0]))
    })
  )
