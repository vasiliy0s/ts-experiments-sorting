import { format } from 'util'
import { Sorter } from '../interfaces'
import { LOG_PREFIX_SIZE } from '../config'

export function sortNPrint(name: string, sorter: Sorter) {
  console.log(
    `${format('%s (origin)', name).padEnd(LOG_PREFIX_SIZE, '.')}%s`,
    sorter.print()
  )

  sorter.sort()

  console.log(
    `${format('%s (sorted)', name).padEnd(LOG_PREFIX_SIZE, '.')}%s`,
    sorter.print()
  )
}
