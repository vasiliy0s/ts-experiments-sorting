import { LOG_PREFIX_SIZE } from '../config'

export const printDivider = (title = '', printNewLine = false): void => {
  title = title ? `{ ${title} }` : ''
  const halfMaxLen = LOG_PREFIX_SIZE
  const midIndex = Math.round(title.length / 2)
  const str = [
    title.slice(0, midIndex).padStart(halfMaxLen, '-'),
    title.slice(midIndex).padEnd(halfMaxLen, '-'),
  ].join('')
  if (printNewLine) console.log('')
  console.log(str)
}
