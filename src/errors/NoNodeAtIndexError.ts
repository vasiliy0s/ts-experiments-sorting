export class NoNodeAtIndexError extends RangeError {
  constructor(nodeIndex: number, stack: string) {
    super(`Cannot find node by index=${nodeIndex}. Current stack="${stack}"`)
  }
}
