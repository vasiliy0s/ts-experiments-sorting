export class OutOfBoundsError extends RangeError {
  constructor(index: number, size: number) {
    super(`Index=${index} is out of bounds (${size})`)
  }
}
