import { format } from 'util'
import { BaseSorter } from './base'
import { LOG_PREFIX_SIZE, PRINT_DEBUG } from '../config'

export class BubbleSort extends BaseSorter {
  sort(): void {
    const colOp = this.collectionOperator
    const { length: len } = colOp

    for (let i = 0; i < len; i++) {
      for (let j = 0; j < len - i - 1; j++) {
        if (colOp.compare(j, j + 1) >= 0) {
          colOp.swap(j, j + 1)
          if (PRINT_DEBUG) this.printSwapped(j, j + 1)
        }
      }
    }
  }

  private printSwapped(i: number, j: number) {
    const prefix = format('swapped %s<->%s:', i, j).padEnd(LOG_PREFIX_SIZE, '.')
    console.log(`${prefix}%s`, this.collectionOperator.print())
  }
}
