import { NativeSorter } from './base'

export class NativeSort<T> extends NativeSorter<T> {
  sort(): void {
    this.collection.sort()
  }
}
