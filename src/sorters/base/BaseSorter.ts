import { CollectionOperator, Sorter } from '../../interfaces'

export abstract class BaseSorter implements Sorter {
  constructor(protected collectionOperator: CollectionOperator) {}

  abstract sort(): void

  print(): string {
    return this.collectionOperator.print()
  }
}
