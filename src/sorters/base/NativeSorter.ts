import { CollectionPrinter } from '../../collection-operators/base'
import { Sorter } from '../../interfaces'

export abstract class NativeSorter<T>
  extends CollectionPrinter<T>
  implements Sorter {
  protected collection: T[]

  abstract sort(): void

  constructor(data: ArrayLike<T>) {
    super()

    this.collection = Array.from(data)
  }
}
