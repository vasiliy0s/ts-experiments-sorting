import { Comparator } from '../interfaces'

export class NumbersComparator implements Comparator<number> {
  compare(a: number, b: number) {
    if (a > b) return 1
    else if (a === b) return 0
    return -1
  }
}
