import { Comparator } from '../interfaces'

export class StringComparator implements Comparator<string> {
  compare(a: string, b: string) {
    const aCharCode = a.charCodeAt(0)
    const bCharCode = b.charCodeAt(0)

    if (aCharCode > bCharCode) return 1
    else if (aCharCode === bCharCode) return 0
    return -1
  }
}
